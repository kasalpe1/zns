#ifndef CITEM_H
#define CITEM_H

#include <QVector>
struct CItem
{
    void calcDistances(const QVector<qreal> & answers);
    QVector<qreal> info;
    QString name;
    qreal euclid;
    int euclidOrder;
    qreal fuzzy;
    int fuzzyOrder;
    qreal cosine;
    int cosineOrder;
};

#endif // CITEM_H
