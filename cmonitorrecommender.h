#ifndef CMONITORRECOMMENDER_H
#define CMONITORRECOMMENDER_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QVector>
#include <QMap>
#include <citem.h>

namespace Ui {
class CMonitorRecommender;
}

class CMonitorRecommender : public QMainWindow
{
    Q_OBJECT

public:
    explicit CMonitorRecommender(QWidget *parent = 0);
    ~CMonitorRecommender();

private:
    Ui::CMonitorRecommender *ui;
    void connectToDb();
    void loadAttributes();
    void loadItems();
    int findEmptyQuestion();
    void setNewQuestion();
    void updateAnswers();
    void updateRecommandation();
    void updateDistances();

    QSqlDatabase db;
    QVector<QPair<QString, bool>> attributes;
    QVector<CItem *> items;

    QVector<qreal> answers;
    int asked = -1;
};

#endif // CMONITORRECOMMENDER_H
