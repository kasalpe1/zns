#include "cmonitorrecommender.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>
#include "ui_cmonitorrecommender.h"
#include <stdlib.h>
#include <time.h>
#include <QShortcut>
#include <QApplication>
#include <QProcess>

CMonitorRecommender::CMonitorRecommender(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CMonitorRecommender)
{
    ui->setupUi(this);
    connectToDb();
    loadAttributes();
    loadItems();
    srand (time(NULL));
    setNewQuestion();
    updateRecommandation();
    connect(ui->skip, &QPushButton::clicked, [&]()
    {
        attributes[asked].second = false;
        setNewQuestion();
    });
    connect(ui->next, &QPushButton::clicked, [&]()
    {
        answers[asked] = ui->slider->value() / 100.0;
        attributes[asked].second = false;
        setNewQuestion();
        updateRecommandation();
        updateAnswers();
    });

    QShortcut * restart = new QShortcut(QKeySequence(Qt::Key_F5), this);
    connect(restart, &QShortcut::activated, [&]()
    {
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    });
}

CMonitorRecommender::~CMonitorRecommender()
{
    delete ui;
}

void CMonitorRecommender::connectToDb()
{
    db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("zns");
    db.setUserName("zns");
    db.setPassword("zns");
    db.open();
}

void CMonitorRecommender::loadAttributes()
{
    QSqlQuery query("SELECT column_name FROM information_schema.columns WHERE table_name = 'monitors'");
    int fieldNo = query.record().indexOf("column_name");
    while (query.next())
    {
        attributes.push_back(QPair<QString, bool>(query.value(fieldNo).toString(), true));
        answers.push_back(-1);
    }
    attributes.removeLast();
    answers.removeLast();
}

void CMonitorRecommender::loadItems()
{
    QSqlQuery query("SELECT * FROM monitors");
    while (query.next())
    {
        CItem * ni = new CItem();
        ni->name = query.value("Name").toString();
        for(int i = 0; i < attributes.length(); i++)
        {
            ni->info.push_back(query.value(i).toDouble());
        }
        items.push_back(ni);
    }
}

int CMonitorRecommender::findEmptyQuestion()
{
    QVector<int> empty;
    for(int i = 0; i < attributes.count(); i++)
    {
        if(attributes[i].second)
            empty.push_back(i);
    }

    if(empty.length() == 0)
        return -1;
    else
        return empty[rand() % empty.length()];
}

void CMonitorRecommender::setNewQuestion()
{
    asked = findEmptyQuestion();
    if(asked == -1)
    {
        ui->question->setText("No more questions");
        ui->questions->setEnabled(false);
    }
    else
    {
        ui->question->setText(attributes[asked].first);
        ui->slider->setValue(50);
        ui->questions->setEnabled(true);
    }
}

void CMonitorRecommender::updateAnswers()
{
    ui->ans->clear();
    int row = 0;
    ui->ans->setRowCount(row);
    for(int i = 0; i < answers.length(); i++)
    {
        if(answers[i] != -1)
        {
            ui->ans->setRowCount(row + 1);
            ui->ans->setItem(row, 0, new QTableWidgetItem(attributes[i].first));
            ui->ans->setItem(row, 1, new QTableWidgetItem(QString::number(answers[i]) + " ("+QString::number(items.first()->info[i]) + ") "));
            row++;
        }
    }
}

void CMonitorRecommender::updateRecommandation()
{
    updateDistances();
    ui->rec->clear();
    QStringList list = {"Euclidean","Cosine","Fuzzy","Final order","Name"};
    ui->rec->setHorizontalHeaderLabels(list);
    for(int i = 0; i < items.length(); i++)
    {
        ui->rec->setItem(i, 0, new QTableWidgetItem(QString::number(items[i]->euclidOrder) + " => " +QString::number(items[i]->euclid)));
        ui->rec->setItem(i, 1, new QTableWidgetItem(QString::number(items[i]->cosineOrder) + " => " +QString::number(items[i]->cosine)));
        ui->rec->setItem(i, 2, new QTableWidgetItem(QString::number(items[i]->fuzzyOrder) + " => " +QString::number(items[i]->euclid)));
        ui->rec->setItem(i, 3, new QTableWidgetItem(QString::number(i+1)));
        ui->rec->setItem(i, 4, new QTableWidgetItem(items[i]->name));
    }
}

void CMonitorRecommender::updateDistances()
{
    for(auto & x : items)
    {
        x->calcDistances(answers);
    }
    std::sort(items.begin(), items.end(), [](const CItem * a, const CItem * b) -> bool
    {
        return a->cosine > b->cosine;
    });
    for(int i = 0; i < items.count(); i++)
    {
        items[i]->cosineOrder = i + 1;
    }

    std::sort(items.begin(), items.end(), [](const CItem * a, const CItem * b) -> bool
    {
        return a->euclid < b->euclid;
    });
    for(int i = 0; i < items.count(); i++)
    {
        items[i]->euclidOrder = i + 1;
    }

    std::sort(items.begin(), items.end(), [](const CItem * a, const CItem * b) -> bool
    {
        return a->fuzzy < b->fuzzy;
    });
    for(int i = 0; i < items.count(); i++)
    {
        items[i]->fuzzyOrder = i + 1;
    }
    std::sort(items.begin(), items.end(), [](const CItem * a, const CItem * b) -> bool
    {
        return (2*a->cosineOrder + a->euclidOrder + a->fuzzyOrder) <= (2*b->cosineOrder + b->euclidOrder + b->fuzzyOrder);
    });
}

