#include "citem.h"
#include <QtMath>
#include <QDebug>

void CItem::calcDistances(const QVector<qreal> & answers)
{
    qreal ab = 0;
    qreal a = 0;
    qreal b = 0;
    qreal c = 0;
    qreal d = 0;
    qreal s = 0;
    for(int i = 0; i < answers.length(); i++)
    {
        if(answers[i] != -1)
        {
            ab += answers[i] * info[i];
            a += pow(answers[i],2);
            b += pow(info[i],2);
            c += answers[i] * info[i];
            s += pow(answers[i] - info[i],2);
        }
        d += info[i];
    }
    if(a == 0 || b == 0)
        cosine = 0;
    else
        cosine = ab / (sqrt(a) * sqrt(b));
    euclid = sqrt(s);
    if(d == 0)
        fuzzy = 0;
    else
        fuzzy = c / d;
}
